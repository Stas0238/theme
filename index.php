<?php 
get_header (); ?>
<div class="content-wrapper">
	<div class="row">
		<div class="main-column">
	<?php if (have_posts()) : ?>
			<?php while (have_posts()) :
				the_post (); ?>
					<div class="post-wrap col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
					<?php get_template_part('content', get_post_format()); ?>
					</div>
			<?php endwhile; ?>
		</div> <!-- /main-column -->
		<?php endif; ?>
		<?php if (is_active_sidebar( 'right-sidebar' )) : ?>
			<div class="sidebar-column">
			</div> <!-- /sidebar-column -->
		<?php endif; ?>
	</div> <!-- /row -->
	<div class="paginate">
		<?php et_posts_will_display(); ?>
		<div class="paginating"><?php echo paginate_links(array ('prev_text' => "<i class='fa fa-angle-left' aria-hidden='true'></i>", 'next_text' => "<i class='fa fa-angle-right' aria-hidden='true'></i>")); ?></div>
	</div>
</div>
<?php get_footer ();
?>