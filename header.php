<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title><?php bloginfo('name'); ?></title>
  <?php wp_head (); ?>
</head>
<body <?php body_class(); ?>>
  <div class="header-wrap">
    <div class="container">
      <header class="main-header">
      <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header logo-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-menu" aria-expanded="false">
            <span class="sr-only">Main menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php et_logo (); ?>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="header-menu">
          <?php $args = array (
              'theme_location' => 'primary',
              'menu_class' => 'nav navbar-nav header-menu'
            );
             wp_nav_menu ($args); ?>
          <div class="navbar-form navbar-right">
            <div class="header-icons">
              <?php get_search_form (); ?>
              <?php if (et_is_woo()) { ?>
              <span class="woocommerce-icon-cart">
              <?php et_wc_cart_count(); 
                    et_cart_dropdown(); ?>
              </span>
              <?php } ?>
            </div>
          </div>
        </div><!-- /.navbar-collapse -->
      </header>
    </div><!-- /.container -->
  </div>
    <div class="container">
      <?php page_heading (); ?>