<?php 
get_header (); ?>
	<div class="row">
		<div class="col-md-12">
	<?php if (have_posts()) :
			the_post (); ?> 
			<div class="post-single post-wrap">
	<?php get_template_part('content', get_post_format()); endif; ?>
			</div>
	<?php if (get_comments_number()) : ?> 
			<div class="posted-comments">
				<div class="comment-title">
					<span class="posted-comments-count"><?php comments_number (); ?> posted</span>
				</div>
			</div>
		<?php endif;
		comments_template (); ?>
		</div>
	</div> <!-- /row -->
<?php get_footer ();
?>