<?php 
/* DEFINE constants */ 
define ('THEMEROOT', get_stylesheet_directory_uri());
define( 'SCRIPTS' , THEMEROOT . '/js' );
define( 'IMAGES' , THEMEROOT . '/images' );
define( 'FRAMEWORK' , get_template_directory() . '/framework' );

// Include init 
require_once( FRAMEWORK . '/init.php' );

// Content width 
if ( function_exists( 'et_content_width' ) ) :
/**
 *
 * Set up the content width value based on the theme's design.
 *
 */

function et_content_width() {
	if ( ! isset( $content_width ) ) {
		$content_width = 800;
	}
}
endif;

// Styles 
if ( !function_exists('et_scripts')) :
	function et_scripts () {
		/* styles css */
		wp_enqueue_style('fontawesome', THEMEROOT . "/css/font-awesome.min.css");
		wp_enqueue_style('bootstrap-styles', THEMEROOT . "/css/bootstrap.min.css");
		wp_enqueue_style('style', THEMEROOT . "/style.css");
		wp_enqueue_style('owl-carousel', THEMEROOT . "/css/owl.carousel.min.css");
		wp_enqueue_style('owl-theme-default', THEMEROOT . "/css/owl.theme.default.css");

		/* scripts js */ 
		wp_register_script( 'bootstrap-js', SCRIPTS . '/bootstrap.min.js', array('jquery'), false, true );
		wp_register_script( 'owl-carousel-js', SCRIPTS . '/owl.carousel.min.js', array('jquery'), false, true );
		wp_register_script( 'main-js', SCRIPTS . '/main.js', array('jquery'), false, true );

		/* load js scripts */ 
		wp_enqueue_script( 'bootstrap-js' );
		wp_enqueue_script('owl-carousel-js');
		wp_enqueue_script('main-js');
	}
	add_action('wp_enqueue_scripts', 'et_scripts');
endif;

// Google Fonts 
if (! function_exists('et_enqueue_google_font')) :
	function et_enqueue_google_font() {

		$query_args = array(
			'family' => 'Open+Sans:400,600,700&Montserrat:300,400,500,500i,600,600i&Poppins:400,500,600&Roboto+Condensed',
			'family' => 'Material+Icons'
		);

		wp_register_style( 'google-fonts', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
		wp_enqueue_style( 'google-fonts' );
	}
add_action ('wp_enqueue_scripts', 'et_enqueue_google_font');
endif;

// Add custom support 
if (! function_exists('et_custom_support')) :

	function et_custom_support () {
	// Navigation Menus
		register_nav_menus(array(
			'primary' => __('Header menu')
			)
		);

		// Enable support for Post Formats.
		add_theme_support( 'post-formats', array(
				'gallery',
				'link',
				'image',
				'quote',
				'video',
				'audio'
		) );

		// Add featured image 
		add_theme_support('post-thumbnails');

		// Woocommerce support
		if (class_exists('WooCommerce')) {
			// add support for woocommerce v3.0
			    add_theme_support( 'wc-product-gallery-zoom' );
			    add_theme_support( 'wc-product-gallery-lightbox' );
			    add_theme_support( 'wc-product-gallery-slider' );
				}
			}
add_action('after_setup_theme', 'et_custom_support');
endif;

// Register sidebars and widgetized areas.

if (!function_exists('et_widgets')) {
	function et_widgets () {
		register_sidebar( array(
		'name'          => __('Right sidebar', 'eighttheme'),
		'id'            => 'Sidebar_1',
		'description'	=> __('Right on the page', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Main-sidebar %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Sidebar shop (left)', 'eighttheme'),
		'id'            => 'shop-sidebar-left',
		'description'	=> __('Sidebar on the shop page on the left side', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Shop-widgets %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Sidebar shop (right)', 'eighttheme'),
		'id'            => 'shop-sidebar-right',
		'description'	=> __('Sidebar on the shop page on the right side', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Shop-widgets %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Prefooter', 'eighttheme'),
		'id'            => 'prefooter',
		'description'	=> __('Above footer', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Prefooter-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('First footer widget area', 'eighttheme'),
		'id'            => 'footer-area-1',
		'description'	=> __('First footer column (paragraph)', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Footer-widget %2$s footer-area-1">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Second footer widget area', 'eighttheme'),
		'id'            => 'footer-area-2',
		'description'	=> __('Second footer column', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Footer-widget %2$s footer-area-2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Third footer widget area', 'eighttheme'),
		'id'            => 'footer-area-3',
		'description'	=> __('Third footer column', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Footer-widget %2$s footer-area-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Fourth footer widget area', 'eighttheme'),
		'id'            => 'footer-area-4',
		'description'	=> __('Fourth footer column', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Footer-widget %2$s footer-area-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Fifth footer widget area', 'eighttheme'),
		'id'            => 'footer-area-5',
		'description'	=> __('Fifth footer column', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Footer-widget %2$s footer-area-5">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __('Footer copyrights', 'eighttheme'),
		'id'            => 'footer-copyrights',
		'description'	=> __('Footer bottom', 'eighttheme'),
		'before_widget' => '<div id="%1$s" class="Footer-bottom-widget %2$s footer-bottom">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded widget-title">',
		'after_title'   => '</h2>',
	) );
	}
	add_action('widgets_init', 'et_widgets');
}
?>