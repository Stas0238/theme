<?php
if (! function_exists('et_posts_carousel')) {
		function et_posts_carousel ($atts) {
			extract(shortcode_atts(array (
				'class' => '',
				'items' => '4',
				'count' => '12',
				'sort' => 'DESC',
				'post_type' => 'post'
				), $atts) );
				$arr = array(
						'class' => $class,
						'items' => $items,
						'count' => $count,
						'sort' => $sort,
						'post_type' => $post_type
					);
				$q = new WP_Query(
				   array( 
				   	'orderby' => 'date', 
				   	'posts_per_page' => $arr['count'], 
				   	'order'  => $arr['sort'], 
				   	'post__not_in' => array(87)
				   )
				); 
				return et_post_slider($q,$arr);
			}
		}
?>