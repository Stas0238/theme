<?php 
if (! function_exists('et_row_shortcode')) {
function et_row_shortcode( $atts, $content ) {
		extract( shortcode_atts( array(
	      	'class' => '',
	      	'background_image' => '',
	      	'height' => ''
		), $atts ) );

	   $extra_class = '';
	   $height_row = '';

	   if ( ! empty( $class ) ) {
	      $extra_class .= ' '. $class;
	   }
	   if (!empty($height)) {
	   	$height_row = "height:" . $height;
	   }
	   	$out  = '';
	   	if(!empty($background_image)) {
	   		$out .= '<div class="row ' . esc_attr( $extra_class ) . '" style="background-image: url('. $background_image . '); background-size: 100%; background-repeat: no-repeat; background-position: center;' . $height_row . '" >';
	   	}
	   	else {
	   		$out .= '<div class="row ' . esc_attr( $extra_class ) . '">';
	   	}
		
		$out .= do_shortcode($content);
		$out .= '</div>';

		return $out;
	}
}
?>