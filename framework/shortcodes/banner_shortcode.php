<?php 
if (! function_exists('et_banner_shortcode')) {
	function et_banner_shortcode ($atts) {
		extract(shortcode_atts(array(
			'extra_class' => '',
			'align' => 'center',
			'fa_icon' => '',
			'img' => '',
			'material_icon' => '',
			'title' => 'Title lorem ipsum',
			'description' => 'Lorem ipsum lorem lorem lorem',
			'background_color' => '#f7f7f7'
			// 'icon_color' => '#fff',
			// 'title_color' => '#fff',
			// 'description_color' => '#fff',
			), $atts));
		 /* $item = '<div style="background-color:'.$background_color.'; text-align:'.$align.'" class = "banner-wrap '. $extra_class . '">'	 .  '<div class="banner-icon" style="color:'.$icon_color.'"><i class="fa fa-'.$icon.'" aria-hidden="true"></i></div>
					<div class="banner-content">
						<h2 class="banner-title" style="color:'.$title_color.'">'.$title.'</h2>
						<div class="banner-description" style="color:'.$description_color.'">'.$description.'</div>
					</div>
				</div>'; */
		$icons = "";
		if (!empty($img)) { 
			if (preg_match('/<img(.*)src(.*)=(.*)"(.*)"/U',$img)) {
				$icons = '<div class="banner-icon">'.$img.'</div>';
			}
			else {
				$icons = '<div class="banner-icon"><img src="'.$img.'" alt="'.get_attachment_id($img).'"></div>';

			}
		}
		elseif(!empty($material_icon)) {
			$icons = '<div class="banner-icon"><i class="material-icons">'.$material_icon.'</i></div>';
		}
		elseif(!empty($fa_icon)) {
			$icons = '<div class="banner-icon"><i class="fa fa-'.$fa_icon.'" aria-hidden="true"></i></div>';
		}
		$item = '<div style="background-color:'.$background_color.'" class = "banner-wrap '. $extra_class . '">'. $icons . '<div class="banner-content">
						<h2 class="banner-title">'.$title.'</h2>
						<div class="banner-description">'.$description.'</div>
					</div>
				</div>';
		return $item;
	}
 } ?>
