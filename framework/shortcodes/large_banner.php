<?php 
if ( ! function_exists('et_large_banner')) {
	function et_large_banner ($atts) {
		$atts = shortcode_atts(array(
			'class' => '',
			'img_url' => '',
			'title' => '',
			'text' => ''
			), $atts);
		$out = '';
		$title = '';
		$img = '';
		if (!empty($atts['title'])) {
			$title = "<h2 class='title'>" . $atts['title'] . "</h2>";
		}
		if (!empty($atts['img_url'])) { 
			if (preg_match('/<img(.*)src(.*)=(.*)"(.*)"/U',$atts['img_url'])) {
				$img = $atts['img_url'];
			}
			else {
				$img = '<img src="'.$atts['img_url'].'" alt="'.get_attachment_id($atts['img_url']).'">';

			}
		}

		$out .= '<div class="large-banner ' . $atts['class'] . '">' . $img . "<div class='banner-text'>" . $title . $atts['text'] . "</div></div>";
		return $out;
	}
}
 ?>
