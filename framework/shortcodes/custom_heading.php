<?php 
if ( ! function_exists('et_custom_heading')) {
	function et_custom_heading ($atts) {
		$atts = shortcode_atts(array(
			'title' => 'Lorem ipsum drol',
			'class' => ''
		), $atts);
		$out = '';
		$out .= "<h2 class='custom-heading ". $atts['class'] . "'>" . $atts['title'] . "</h2>";
		return $out;
	}
}
 ?>
