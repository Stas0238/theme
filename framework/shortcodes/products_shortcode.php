<?php 
		if (! function_exists('related_products')) {
			function related_products( $atts ) {
				$atts = shortcode_atts( array(
					'per_page' => '5',
					'columns'  => '4',
					'orderby'  => 'rand',
				), $atts, 'related_products' );
				$list = '';
				$arr = array (
					'per_page' => $atts['per_page'],
					'columns' => $atts['columns'],
					'orderby' => $atts['orderby']
					); 
				ob_start();

				// Rename arg
				$arr['posts_per_page'] = absint( $arr['per_page'] );			

				woocommerce_related_products( $arr );

				$list .= ob_get_clean();
				$list .= '<script type="text/javascript"> jQuery(document).ready(function() {
				   jQuery("#products-carousel").owlCarousel({
				       autoPlay: 3000,  
					 	  margin: 10,
					 	  items: 3,
					      itemsDesktop : [1199,3],
					      itemsDesktopSmall : [979,3],
					      navText: ["<i class=\'fa fa-angle-left\' aria-hidden=\'true\'></i>", "<i class=\' fa fa-angle-right \' aria-hidden=\'true\'></i>"],
					      nav: true,
					      dots : false,
					      responsive: {
						      	0:{
					               items:1,
					           	},
					           	600:{
					               items: 4,
					           	},
					           	1000:{
					               items: ' . $arr['columns'] .',
					               loop:false
					           	}
					      }
				   });
				  });
				</script>';
				return $list;
			}
		}
?>