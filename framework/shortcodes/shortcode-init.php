<?php
require_once(FRAMEWORK . '/shortcodes/row_shortcode.php');
require_once(FRAMEWORK . '/shortcodes/col_shortcode.php');
require_once(FRAMEWORK . '/shortcodes/posts_carousel_shortcode.php');
require_once(FRAMEWORK . '/shortcodes/banner_shortcode.php');
require_once(FRAMEWORK . '/shortcodes/container_shortcode.php');
require_once(FRAMEWORK . '/shortcodes/products_shortcode.php');
require_once(FRAMEWORK . '/shortcodes/custom_heading.php');
require_once(FRAMEWORK . '/shortcodes/large_banner.php');
require_once(FRAMEWORK . '/shortcodes/share_buttons_shortcode.php');
if (! function_exists('register_shortcodes')) {
	function register_shortcodes () {
		add_shortcode('row_shortcode', 'et_row_shortcode');
		add_shortcode('col_shortcode', 'et_col_shortcode');
		add_shortcode('posts_carousel', 'et_posts_carousel');
		add_shortcode('banner_shortcode', 'et_banner_shortcode');
		add_shortcode('container', 'et_container_shortcode');
		add_shortcode('products_carousel', 'related_products');
		add_shortcode('custom_heading', 'et_custom_heading');
		add_shortcode('large_banner', 'et_large_banner');
		add_shortcode('share_buttons', 'et_share_buttons_shortcode');
	}
	add_action('init', 'register_shortcodes');
}
?>