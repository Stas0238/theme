<?php 
if ( ! function_exists('et_share_buttons_shortcode')) {
	function et_share_buttons_shortcode () {
		$share = '<ul class="post-social">
		<li><a href="http://www.facebook.com/share.php?u=' . esc_url(get_permalink()) . '"><i class="fa fa-facebook"></i></a></li>
		<li><a href="http://twitter.com/home?' . esc_url(get_permalink()) . '"><i class="fa fa-twitter"></i></a></li>
		<li><a href="https://plus.google.com/share?url=' . esc_url(get_permalink()) . '"><i class="fa fa-google-plus"></i></a></li>
		<li><a href="mailto:enteryour@addresshere.com?subject=' . get_the_title() . 'body=Check%20this%20out:%20' . esc_url(get_permalink()) . '" target="_blank"><i class="fa fa-envelope"></i></a></li>
		<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=' . esc_url(get_permalink()) . '&summary=&source=' . get_home_url() . '"><i class="fa fa-linkedin"></i></a></li>
	</ul>';
		return $share;
	} 
}
?>
