<?php 
if (! function_exists('et_col_shortcode')) {
	function et_col_shortcode( $atts, $content ) {
		extract(shortcode_atts( array(
	      	'class' => '',
	      	'width' => ''

		), $atts ));
		extract(shortcode_atts( array(
	      	'lg' => "$width",
	      	'md' => "$width",
	      	'sm' => "$width"

		), $atts ));

	   $extra_class = '';

	   switch ($width) {
	   	case '1/12'; case '1':
	   		$extra_class = '1';
	   		break;
	   	case '1/6'; case '2':
	   		$extra_class = '2';
	   		break;
	   	case '1/3'; case '3':
	   		$extra_class = '4';
	   		break;
		case '1/4'; case '4':
	   		$extra_class = '3';
	   		break;
	   	case '5/12'; case '5':
	   		$extra_class = '5';
	   		break;
		case '1/2'; case '6':
	   		$extra_class = '6';
	   		break;
		case '7/12'; case '7':
	   		$extra_class = '7';
	   		break;
	   	case '2/3'; case '8':
	   		$extra_class = '8';
	   		break;
	   	case '3/4'; case '9':
	   		$extra_class = '9';
	   		break;
	   	case '5/6'; case '10':
	   		$extra_class = '10';
	   		break;
	   	case '11/12'; case '11':
	   		$extra_class = '11';
	   		break;
	   	case '1/1'; case '12':
	   		$extra_class = '12';
	   		break;
	   	default:
	   		$extra_class = '4';
	   		break;
	   }
	   if ($lg == '1/12' | $lg == '1') :
	   		$lg = '1';
	   	elseif ($lg == '1/6' | $lg == '2') :
	   		$lg = '2';
	   	elseif ($lg == '1/3' | $lg == '3') : 
	   		$lg = '3';
	   	elseif ($lg == '1/4' | $lg == '4') : 
	   		$lg = '4';
	   	elseif ($lg == '5/12' | $lg == '5') : 
	   		$lg = '5';
	   	elseif ($lg == '1/2' | $lg == '6') : 
	   		$lg = '6';
	   	elseif ($lg == '7/12' | $lg == '7') : 
	   		$lg = '7';
	   	elseif ($lg == '2/3' | $lg == '8') : 
	   		$lg = '8';
	   	elseif ($lg == '3/4' | $lg == '9') : 
	   		$lg = '9';
	   	elseif ($lg == '5/6' | $lg == '10') : 
	   		$lg = '10';
	   	elseif ($lg == '11/12' | $lg == '11') : 
	   		$lg = '11';
	   	elseif ($lg == '1/1' | $lg == '12') : 
	   		$lg = '12';
	   	else : 
	   		$lg = '4';
	   	endif;
	   	if ($md == '1/12' | $md == '1') :
	   		$md = '1';
	   	elseif ($md == '1/6' | $md == '2') :
	   		$md = '2';
	   	elseif ($md == '1/3' | $md == '3') : 
	   		$md = '3';
	   	elseif ($md == '1/4' | $md == '4') : 
	   		$md = '4';
	   	elseif ($md == '5/12' | $md == '5') : 
	   		$md = '5';
	   	elseif ($md == '1/2' | $md == '6') : 
	   		$md = '6';
	   	elseif ($md == '7/12' | $md == '7') : 
	   		$md = '7';
	   	elseif ($md == '2/3' | $md == '8') : 
	   		$md = '8';
	   	elseif ($md == '3/4' | $md == '9') : 
	   		$md = '9';
	   	elseif ($md == '5/6' | $md == '10') : 
	   		$md = '10';
	   	elseif ($md == '11/12' | $md == '11') : 
	   		$md = '11';
	   	elseif ($md == '1/1' | $md == '12') : 
	   		$md = '12';
	   	else : 
	   		$md = '4';
	   	endif;
	   	if ($sm == '1/12' | $sm == '1') :
	   		$sm = '1';
	   	elseif ($sm == '1/6' | $sm == '2') :
	   		$sm = '2';
	   	elseif ($sm == '1/3' | $sm == '3') : 
	   		$sm = '3';
	   	elseif ($sm == '1/4' | $sm == '4') : 
	   		$sm = '4';
	   	elseif ($sm == '5/12' | $sm == '5') : 
	   		$sm = '5';
	   	elseif ($sm == '1/2' | $sm == '6') : 
	   		$sm = '6';
	   	elseif ($sm == '7/12' | $sm == '7') : 
	   		$sm = '7';
	   	elseif ($sm == '2/3' | $sm == '8') : 
	   		$sm = '8';
	   	elseif ($sm == '3/4' | $sm == '9') : 
	   		$sm = '9';
	   	elseif ($sm == '5/6' | $sm == '10') : 
	   		$sm = '10';
	   	elseif ($sm == '11/12' | $sm == '11') : 
	   		$sm = '11';
	   	elseif ($sm == '1/1' | $sm == '12') : 
	   		$sm = '12';
	   	else : 
	   		$sm = '4';
	   	endif;
	   	$out  = '';
		$out .= '<div class="' . esc_attr( $class ). ' col-lg-' . esc_attr( $lg ). ' col-md-' . esc_attr( $md ) . ' col-sm-'. esc_attr( $sm ). ' col-xs-'.esc_attr( $extra_class ).'">';
		$out .= do_shortcode($content);
		$out .= '</div>';

		return $out;
	}
}
 ?>