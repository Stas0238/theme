<?php 
if (! function_exists('et_slide_content')) {
	function et_slide_content ($atts) {
		extract(shortcode_atts(array(
			'class' => '',
			'product_name' => '',
			'title' => '',
			'description' => '',
			'href' => '',
			'buttonText' => ''
			), $atts));
			extract(shortcode_atts(array (
				'before' => '<div class="' . $class . '">',
				'after' => '<div>',
			), $atts));
			$list = $before . '<h5>'. $product_name .'</h5>'. '<h2>' . $title .'</h2>' . '<div class="span">' . $description . '</div>' . '<a href="'. $href . '">' . $buttonText . '</a>' . $after ;
			return $list;
	}
 } ?>
