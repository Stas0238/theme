<?php 
// Logo 
if (! function_exists('et_get_logo')) {
	function et_get_logo () {
		echo "<a class='brand' href='". home_url () . "'> <img src='" . IMAGES . "/logo.png" . "' alt=''>" . "</a>";
	}
}

// Page heading including breadcrumbs 
if (!function_exists('page_heading')) :
	function page_heading () {
		if ((!is_page('home')) && (is_woocommerce() == false) && !is_page('contact-us')) {
			echo "<div class='page-heading'>";
			echo "<div class='bc-title'>"; 
			wp_title("");
			echo "</div>"; ?>
		      <a class="bc-return" href="javascript: history.go(-1)"><i class='fa fa-angle-left' aria-hidden='true'></i>  <?php esc_html_e('Return to Previous Page', 'eighttheme'); ?></a> 
		      <?php et_custom_breadcrumbs(); 
		      echo "</div>"; ?>
		<?php }
		 }
endif;

if ( ! function_exists( 'et_share_buttons' ) ) :

/* Function includes social share buttons */ 

function et_share_buttons() { ?>
	<ul class="post-social">
		<li><a href="http://www.facebook.com/share.php?u=<?php esc_url(the_permalink()); ?> /&title=<?php the_title(); ?>"><i class="fa fa-facebook"></i></a></li>
		<li><a href="http://twitter.com/home?status=<?php the_title(); ?>+<?php esc_url(the_permalink()); ?>"><i class="fa fa-twitter"></i></a></li>
		<li><a href="https://plus.google.com/share?url=<?php esc_url(the_permalink()); ?>"><i class="fa fa-google-plus"></i></a></li>
		<li><a href="mailto:enteryour@addresshere.com?subject=<?php urlencode(the_title()); ?>body=Check%20this%20out:%20<?php esc_url(the_permalink()); ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
		<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php esc_url(the_permalink()); ?>&title=<?php the_title(); ?>&summary=&source=<?php get_home_url(); ?>"><i class="fa fa-linkedin"></i></a></li>
	</ul>

<?php 
} 
endif;

// Author's avatar in link 

if (!function_exists('et_post_author_avatar_link')) :
	function et_post_author_avatar_link() {
	printf( '<a href="%1$s" rel="author">%2$s</a>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_avatar(get_the_author_meta('ID'), "130x130"));						 
	}
endif;

// Author's name in link 

if (!function_exists('et_post_author_name_link')) {
	function et_post_author_name_link () {
		printf( '<a href="%1$s" rel="author">%2$s</a>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_the_author() );
	}
}
// Post comments 

if (!function_exists('et_post_comments')) :
	function et_post_comments ($comment, $args, $depth) { ?>
    <?php $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	<div id="comment-<?php comment_ID(); ?>">
	<div class="comment-author-avatar vcard">
	<?php // $id = comment_ID (); 
	// 	$comment = comment_author_url($id); echo $comment; ?>
		<div rel="author"><?php echo get_avatar( $comment, $size='80'); ?></div>
	</div>
	<?php if ($comment->comment_approved == '0') : ?>
		<em><?php esc_html_e('Your cooment has been checking.', 'eighttheme'); ?></em>
		<br />
	<?php endif; ?>

		<div class="comment-meta commentmetadata">
			<div class="comment-author-name" ><?php comment_author() ?></div>
			<div class="comment-time"><?php printf( '%1$s - %2$s', get_comment_date(),  get_comment_time()) ?></div>
			<div class="comment-text"><?php comment_text() ?></div>
			<div class="comment-reply">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => 'Reply to comment'))) ?></div>
		</div>
	</div>
	<?php }
endif;

// Showing count post per page with hyphen 
if (!function_exists('et_posts_will_display')) : 
	function et_posts_will_display () { ?>
	<div class="showing-posts">
		<?php
		global $wp_query;
		$paged    = max( 1, $wp_query->get( 'paged' ) );
		$per_page = $wp_query->get( 'posts_per_page' );
		$total    = $wp_query->found_posts;
		$first    = ( $per_page * $paged ) - $per_page + 1;
		$last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );
		if ( $total <= $per_page || -1 === $per_page ) {
			/* translators: %d: total results */
			printf( _n( 'Showing the single result', 'Showing all %d results', $total, 'eighttheme_posts'), $total );
		} else {
			/* translators: 1: first result 2: last result 3: total results */
			printf( _n('Showing the single result', 'Showing <span class="posts-count">%1$d&ndash;%2$d of %3$d posts</span>', $total, 'eighttheme_posts' ), $first, $last, $total );
		}
		?>
	</div>
<?php } endif;

// Function post gallery slider 

if (!function_exists('et_post_gallery_slider'))  :
function et_post_gallery_slider() { ?>
	<?php global $post; ?>
	<?php $id= get_the_ID(); ?>
		<div id="et-galery-<?php echo $id; ?>" class="galery-carousel owl-carousel owl-theme overflow-navs">

			<?php $gal = get_post_gallery($id, false); 
				$alt_id = $gal['ids'];
				$ex = explode(",", $alt_id); // print_r($ex);
			 $gallery = get_post_gallery_images( $post ); $i = 0; ?>
			<?php foreach( $gallery as $image_url ) : ?>
				<div class="item">
					<img data-src="<?php echo $image_url; ?>" alt='<?php echo get_post_meta( $ex[$i], "_wp_attachment_image_alt", true); ?>' class="owl-lazy">
				</div><!-- End .item -->
			<?php $i++; endforeach; ?>

		</div><!-- End #et-galery .galery-carousel -->

	<script type="text/javascript">
		jQuery(document).ready(function() {
		 
		   jQuery("#et-galery-<?php echo $id; ?>").owlCarousel({
		       loop:true,
		       margin:10,
		       responsiveClass:true,
		       dots: true,
		       lazyLoad: true,
		       autoplay:false,
		       pagination:true,
		       navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		       responsive:{
		           0:{
		               items:1,
		               nav:true
		           },
		           600:{
		               items:1,
		               nav:false
		           },
		           1000:{
		               items:1,
		               nav:true,
		               loop:false
		           }
		       }
		   })
		 
		});
	</script>
<?php
}
endif;

// Excrete blockquote on quote post format 

function my_quote_content() {

	/* Check if we're displaying a 'quote' post. */

		/* Match any <blockquote> elements. */
		preg_match_all('#<blockquote>(.+?)</blockquote>#is', get_the_content(), $arr);
		$quote = $arr[0][0];
		$quote = strip_tags($quote, '<blockquote>');
		$quote = strip_tags($quote, '</blockquote>');
		/* If no <blockquote> elements were found, wrap the entire content in one. */

	return $quote;
}

// Default woocommerce styles 
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

if (! function_exists('et_post_slider')) {
	function et_post_slider ($q, $atts) {
		$rand = rand(1,9999);
		$list = '<div class="'. esc_attr($atts['class']) . '"><h2 class="custom-heading text-center">Latest News</h2><div id="post-carousel-'.$rand.'" class="owl-carousel owl-theme posts-carousel">';
				while($q->have_posts()) : 
				$q->the_post();
				ob_start ();
				get_template_part('content', get_post_format());
				$output = ob_get_clean();

				// if (get_post_format() != "gallery" && get_post_format() != "link") {

				$list .=  '<div class="item"><div class="post-wrap text-center">' . $output . '</div></div>';

				// }
				endwhile;
				$list .= "</div></div>";
				$list .= '<script> jQuery(document).ready(function() {
				   jQuery("#post-carousel-'.$rand.'").owlCarousel({
				       margin:30,
				       responsiveClass:true,
				       lazyLoad: true,
				       autoplay:false,
				       dots: true,
				       responsive:{
				           0:{
				               items:1,
				           },
				           600:{
				               items: 3,
				           },
				           1000:{
				               items: ' . $atts['items'] . ',
				               loop:false
				           }
				       }
				   })
				});
				</script>';
			wp_reset_query(); ?>
				<?php return $list;
	}
}
function get_attachment_id( $url ) {
	$attachment_id = 0;
	$dir = wp_upload_dir();
	if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
		$file = basename( $url );
		$query_args = array(
			'post_type'   => 'attachment',
			'post_status' => 'inherit',
			'fields'      => 'ids',
			'meta_query'  => array(
				array(
					'value'   => $file,
					'compare' => 'LIKE',
					'key'     => '_wp_attachment_metadata',
				),
			)
		);
		$query = new WP_Query( $query_args );
		if ( $query->have_posts() ) {
			foreach ( $query->posts as $post_id ) {
				$meta = wp_get_attachment_metadata( $post_id );
				$original_file       = basename( $meta['file'] );
				$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
				if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
					$attachment_id = $post_id;
					break;
				}
			}
		}
	}
	return $attachment_id;
}
if (! function_exists('et_is_woo')) :
	function et_is_woo(){
		return class_exists( 'WooCommerce' );
	}
endif;

// Logo 
if (! function_exists('et_logo')) {
	function et_logo () {
		printf('<a class="brand" href="%s"><img src="%s/logo.png" alt=""></a>', home_url(), IMAGES);
	}
}

// excerpt length 
if (! function_exists('et_custom_excerpt')) :
	function et_custom_excerpt () {
		return 25;
	}
	add_filter ('excerpt_length', 'et_custom_excerpt');
endif;

// Breadcrumbs 

/**
 * Breadcrumbs for WordPress (breadcrumbs)
 *
 * @param  string [$sep  = '']      Divider. Default ' » '
 * @param  array  [$l10n = array()] For localization. Look variable $default_l10n.
 * @param  array  [$args = array()] Options. Look variable $def_args
 * @return string Output on screen HTML cod
 *
 * version 3.3.1
 */
if (!function_exists('et_custom_breadcrumbs')) {
	function et_custom_breadcrumbs( $sep = ' / ', $l10n = array(), $args = array() ){
		$kb = new Kama_Breadcrumbs;
		echo $kb->get_crumbs( $sep, $l10n, $args );
	}
}

class Kama_Breadcrumbs {

	public $arg;

	// Локализация
	static $l10n = array(
		'home'       => 'Home',
		'paged'      => 'Page %d',
		'_404'       => 'Error 404',
		'search'     => 'Search results for - <b>%s</b>',
		'author'     => 'Author archive: <b>%s</b>',
		'year'       => 'Archive for <b>%d</b> year',
		'month'      => 'Archive for: <b>%s</b>',
		'day'        => '',
		'attachment' => 'Media: %s',
		'tag'        => 'Entries by tag: <b>%s</b>',
		'tax_tag'    => '%1$s from "%2$s" by tag: <b>%3$s</b>',
		// tax_tag outputs: 'type_of_note from "name_tags" by tag: name_of_term'.
	);

	// Default parametrs
	static $args = array(
		'on_front_page'   => true,  // Outputs crumbs on the main page 
		'show_post_title' => true,  // Whether to provide the title recording at the end (last element). For the records, pages, attachments
		'show_term_title' => true,  // whether to show the name of the taxonomy element at the end (last element). For labels, headings, and other taxes
		'title_patt'      => '<span class="kb_title">%s</span>', // template for the final title. If enabled: show_post_title or show_term_title
		'last_sep'        => true,  // to show last divider, when the title does not show in the end 
		'markup'          => 'schema.org', // 'markup' - markup. May be: 
		// 'rdf.data-vocabulary.org', 'schema.org', '' - without markup 
										   // whether to write one's own massive of markup 
										   // array( 'wrappatt'=>'<div class="kama_breadcrumbs">%s</div>', 'linkpatt'=>'<a href="%s">%s</a>', 'sep_after'=>'', )
		'priority_tax'    => array('category'), // primary taxonomy, when it needs notes in several taxes 
		'priority_terms'  => array(), // 'priority_terms' - taxonomies priority elements when the record is a few elements of a fee at the same time.
									  // Example: array( 'category'=>array(45,'term_name'), 'tax_name'=>array(1,2,'name') )
									  // 'category' - tax which has elements : 45 - ID of term and 'term_name' - label.
									  // order 45 and 'term_name' have means: earlier more important. All indicated taxes more important than not indicated...
		'nofollow' => false, // add rel=nofollow to links ?

		// служебные
		'sep'             => '',
		'linkpatt'        => '',
		'pg_end'          => '',
	);

	function get_crumbs( $sep, $l10n, $args ){
		global $post, $wp_query, $wp_post_types;

		self::$args['sep'] = $sep;

		// Filters defaults and writes
		$loc = (object) array_merge( apply_filters('kama_breadcrumbs_default_loc', self::$l10n ), $l10n );
		$arg = (object) array_merge( apply_filters('kama_breadcrumbs_default_args', self::$args ), $args );

		$arg->sep = '<span class="kb_sep">'. $arg->sep .'</span>'; // add 

		// easier 
		$sep = & $arg->sep;
		$this->arg = & $arg;

		// micromarkup ---
		if(1){
			$mark = & $arg->markup;

			// Default markup 
			if( ! $mark ) $mark = array(
				'wrappatt'  => '<div class="kama_breadcrumbs">%s</div>',
				'linkpatt'  => '<div class="kama_title"><a href="%s">%s</a>',
				'sep_after' => '',
			);
			// rdf
			elseif( $mark === 'rdf.data-vocabulary.org' ) $mark = array(
				'wrappatt'   => '<div class="kama_breadcrumbs" prefix="v: http://rdf.data-vocabulary.org/#">%s</div>',
				'linkpatt'   => '<span typeof="v:Breadcrumb"><a href="%s" rel="v:url" property="v:title">%s</a>',
				'sep_after'  => '</span>', // close span right away after divider!
			);
			// schema.org
			elseif( $mark === 'schema.org' ) $mark = array(
				'wrappatt'   => '<div class="kama_breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">%s</div>',
				'linkpatt'   => '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="%s" itemprop="item"><span itemprop="name">%s</span></a></span>',
				'sep_after'  => "",
			);

			elseif( ! is_array($mark) )
				die( __CLASS__ .': "markup" parameter must be array...');

			$wrappatt  = $mark['wrappatt'];
			$arg->linkpatt  = $arg->nofollow ? str_replace('<a ','<a rel="nofollow"', $mark['linkpatt']) : $mark['linkpatt'];
			$arg->sep      .= $mark['sep_after']."\n";
		}

		$linkpatt = $arg->linkpatt; // easier 

		$q_obj = get_queried_object();

		// if this zip of empty tax ?
		$ptype = null;
		if( empty($post) ){
			if( isset($q_obj->taxonomy) )
				$ptype = & $wp_post_types[ get_taxonomy($q_obj->taxonomy)->object_type[0] ];
		}
		else $ptype = & $wp_post_types[ $post->post_type ];

		// paged
		$arg->pg_end = '';
		if( ($paged_num = get_query_var('paged')) || ($paged_num = get_query_var('page')) )
			$arg->pg_end = $sep . sprintf( $loc->paged, (int) $paged_num );

		$pg_end = $arg->pg_end; // easier 

		// let's with God ... 
		$out = '';

		if( is_front_page() ){
			return $arg->on_front_page ? sprintf( $wrappatt, ( $paged_num ? sprintf($linkpatt, get_home_url(), $loc->home) . $pg_end : $loc->home ) ) : '';
		}
		// page records, where the main page is another 
		elseif( is_home() ) {
			$out = $paged_num ? ( sprintf( $linkpatt, get_permalink($q_obj), esc_html($q_obj->post_title) ) . $pg_end ) : esc_html($q_obj->post_title);
		}
		elseif( is_404() ){
			$out = $loc->_404;
		}
		elseif( is_search() ){
			$out = sprintf( $loc->search, esc_html( $GLOBALS['s'] ) );
		}
		elseif( is_author() ){
			$tit = sprintf( $loc->author, esc_html($q_obj->display_name) );
			$out = ( $paged_num ? sprintf( $linkpatt, get_author_posts_url( $q_obj->ID, $q_obj->user_nicename ) . $pg_end, $tit ) : $tit );
		}
		elseif( is_year() || is_month() || is_day() ){
			$y_url  = get_year_link( $year = get_the_time('Y') );

			if( is_year() ){
				$tit = sprintf( $loc->year, $year );
				$out = ( $paged_num ? sprintf($linkpatt, $y_url, $tit) . $pg_end : $tit );
			}
			// month day
			else {
				$y_link = sprintf( $linkpatt, $y_url, $year);
				$m_url  = get_month_link( $year, get_the_time('m') );

				if( is_month() ){
					$tit = sprintf( $loc->month, get_the_time('F') );
					$out = $y_link . $sep . ( $paged_num ? sprintf( $linkpatt, $m_url, $tit ) . $pg_end : $tit );
				}
				elseif( is_day() ){
					$m_link = sprintf( $linkpatt, $m_url, get_the_time('F'));
					$out = $y_link . $sep . $m_link . $sep . get_the_time('l');
				}
			}
		}

		elseif( is_singular() && $ptype->hierarchical ){
			$out = $this->_add_title( $this->_page_crumbs($post), $post );
		}
		// Taxes, records and attachments
		else {
			$term = $q_obj; // taxonomy 

			// define the term for notes (including attachments)
			if( is_singular() ){
				// change $post, to define the term of parent attachments 
				if( is_attachment() && $post->post_parent ){
					$save_post = $post; // сохраним
					$post = get_post($post->post_parent);
				}

				// It takes into account if the investments are attached to the tree-taxi
				$taxonomies = get_object_taxonomies( $post->post_type );
				// public notes 
				$taxonomies = array_intersect( $taxonomies, get_taxonomies( array('hierarchical' => true, 'public' => true) ) );

				if( $taxonomies ){
					// order by priory 
					if( ! empty($arg->priority_tax) ){
						usort( $taxonomies, function($a,$b)use($arg){
							$a_index = array_search($a, $arg->priority_tax);
							if( $a_index === false ) $a_index = 9999999;

							$b_index = array_search($b, $arg->priority_tax);
							if( $b_index === false ) $b_index = 9999999;

							return ( $b_index === $a_index ) ? 0 : ( $b_index < $a_index ? 1 : -1 ); 
						} );
					}

					// try to get terms, in order by priory of taxes 
					foreach( $taxonomies as $taxname ){
						if( $terms = get_the_terms( $post->ID, $taxname ) ){
							// chech priory terms for tax 
							$prior_terms = & $arg->priority_terms[ $taxname ];
							if( $prior_terms && count($terms) > 2 ){
								foreach( (array) $prior_terms as $term_id ){
									$filter_field = is_numeric($term_id) ? 'term_id' : 'slug';
									$_terms = wp_list_filter( $terms, array($filter_field=>$term_id) );

									if( $_terms ){
										$term = array_shift( $_terms );
										break;
									}
								}
							}
							else
								$term = array_shift( $terms );

							break;
						}
					}
				}

				if( isset($save_post) ) $post = $save_post; // return (for attachments)
			}

			// Output 

			// all types of notes with terms or terms 
			if( $term && isset($term->term_id) ){
				$term = apply_filters('kama_breadcrumbs_term', $term );

				// attachment
				if( is_attachment() ){
					if( ! $post->post_parent )
						$out = sprintf( $loc->attachment, esc_html($post->post_title) );
					else {
						if( ! $out = apply_filters('attachment_tax_crumbs', '', $term, $this ) ){
							$_crumbs    = $this->_tax_crumbs( $term, 'self' );
							$parent_tit = sprintf( $linkpatt, get_permalink($post->post_parent), get_the_title($post->post_parent) );
							$_out = implode( $sep, array($_crumbs, $parent_tit) );
							$out = $this->_add_title( $_out, $post );
						}
					}
				}
				// single
				elseif( is_single() ){
					if( ! $out = apply_filters('post_tax_crumbs', '', $term, $this ) ){
						$_crumbs = $this->_tax_crumbs( $term, 'self' );
						$out = $this->_add_title( $_crumbs, $post );
					}
				}
				// marks 
				elseif( ! is_taxonomy_hierarchical($term->taxonomy) ){
					// mark
					if( is_tag() )
						$out = $this->_add_title('', $term, sprintf( $loc->tag, esc_html($term->name) ) );
					// tax 
					elseif( is_tax() ){
						$post_label = $ptype->labels->name;
						$tax_label = $GLOBALS['wp_taxonomies'][ $term->taxonomy ]->labels->name;
						$out = $this->_add_title('', $term, sprintf( $loc->tax_tag, $post_label, $tax_label, esc_html($term->name) ) );
					}
				}
				// rubric 
				else {
					if( ! $out = apply_filters('term_tax_crumbs', '', $term, $this ) ){
						$_crumbs = $this->_tax_crumbs( $term, 'parent' );
						$out = $this->_add_title( $_crumbs, $term, esc_html($term->name) );                     
					}
				}
			}
			// attechments for note from terms 
			elseif( is_attachment() ){
				$parent = get_post($post->post_parent);
				$parent_link = sprintf( $linkpatt, get_permalink($parent), esc_html($parent->post_title) );
				$_out = $parent_link;

				// attechments from tree-type note 
				if( is_post_type_hierarchical($parent->post_type) ){
					$parent_crumbs = $this->_page_crumbs($parent);
					$_out = implode( $sep, array( $parent_crumbs, $parent_link ) );
				}

				$out = $this->_add_title( $_out, $post );
			}
			// notes without attechments 
			elseif( is_singular() ){
				$out = $this->_add_title( '', $post );
			}
		}

		// replace links into zip page for type of note 
		$home_after = apply_filters('kama_breadcrumbs_home_after', '', $linkpatt, $sep, $ptype );

		if( '' === $home_after ){
			// Link to the archive page for the record type: the individual pages of this type; files of this type; taxonomies associated with this type.
			if( $ptype && $ptype->has_archive && ! in_array( $ptype->name, array('post','page','attachment') )
				&& ( is_post_type_archive() || is_singular() || (is_tax() && in_array($term->taxonomy, $ptype->taxonomies)) )
			){
				$pt_title = $ptype->labels->name;

				// the first zip page of notes type 
				if( is_post_type_archive() && ! $paged_num )
					$home_after = $pt_title;
				// singular, paged post_type_archive, tax
				else{
					$home_after = sprintf( $linkpatt, get_post_type_archive_link($ptype->name), $pt_title );

					$home_after .= ( ($paged_num && ! is_tax()) ? $pg_end : $sep ); // pagination
				}
			}
		}

		$before_out = sprintf( $linkpatt, home_url(), $loc->home ) . ( $home_after ? $sep.$home_after : ($out ? $sep : '') );

		$out = apply_filters('kama_breadcrumbs_pre_out', $out, $sep, $loc, $arg );

		$out = sprintf( $wrappatt, $before_out . $out );

		return apply_filters('kama_breadcrumbs', $out, $sep, $loc, $arg );
	}

	function _page_crumbs( $post ){
		$parent = $post->post_parent;

		$crumbs = array();
		while( $parent ){
			$page = get_post( $parent );
			$crumbs[] = sprintf( $this->arg->linkpatt, get_permalink($page), esc_html($page->post_title) );
			$parent = $page->post_parent;
		}

		return implode( $this->arg->sep, array_reverse($crumbs) );
	}

	function _tax_crumbs( $term, $start_from = 'self' ){
		$termlinks = array();
		$term_id = ($start_from === 'parent') ? $term->parent : $term->term_id;
		while( $term_id ){
			$term       = get_term( $term_id, $term->taxonomy );
			$termlinks[] = sprintf( $this->arg->linkpatt, get_term_link($term), esc_html($term->name) );
			$term_id    = $term->parent;
		}

		if( $termlinks )
			return implode( $this->arg->sep, array_reverse($termlinks) ) /*. $this->arg->sep*/;
		return '';
	}

	// add title for given text , considering all options. Adds a separator to the top if necessary.
	function _add_title( $add_to, $obj, $term_title = '' ){
		$arg = & $this->arg; // упростим...
		$title = $term_title ? $term_title : esc_html($obj->post_title); // $term_title is cleaned separately 
		$show_title = $term_title ? $arg->show_term_title : $arg->show_post_title;

		// pagination
		if( $arg->pg_end ){
			$link = $term_title ? get_term_link($obj) : get_permalink($obj);
			$add_to .= ($add_to ? $arg->sep : '') . sprintf( $arg->linkpatt, $link, $title ) . $arg->pg_end;
		}
		// add - set sep 
		elseif( $add_to ){
			if( $show_title )
				$add_to .= $arg->sep . sprintf( $arg->title_patt, $title );
			elseif( $arg->last_sep )
				$add_to .= $arg->sep;
		}
		// sep is then ...
		elseif( $show_title )
			$add_to = sprintf( $arg->title_patt, $title );

		return $add_to;
	}

}

// end et_custom_breadcrumbs 

if ( ! function_exists( 'et_move_comment_field_to_bottom' ) ) :

	function et_move_comment_field_to_bottom( $fields ) {
	  /**
	  * Move comment field to bottom.
	  */
	  $comment_field = $fields['comment'];
	  unset( $fields['comment'] );
	  $fields['comment'] = $comment_field;
	  return $fields;
	}

	add_filter( 'comment_form_fields', 'et_move_comment_field_to_bottom' );
	endif;

// Woocommerce 
	if (et_is_woo()) {
		//to be inserted into functions.php, not instead of!
		//makes cart count updates when products are added to the cart via AJAX
		add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment', 'woocommerce_header_add_to_mini_cart_fragment' );
		    function woocommerce_header_add_to_cart_fragment( $fragments ) {ob_start();?>
		        <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'eighttheme'); ?>"><?php echo sprintf (_n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></a>
		    <?php   
		    $fragments['a.cart-contents'] = ob_get_clean();
		    return $fragments;
		}
		//makes mini-cart contents update when products are added to the cart via AJAX
		add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_mini_cart_fragment' );
		    function woocommerce_header_add_to_mini_cart_fragment( $mini_cart ) {ob_start();?>
		    <div class="mini-cart-inner">
		        <?php
		        global $woocommerce;
		            $items = $woocommerce->cart->get_cart();
		            $currency = get_woocommerce_currency_symbol();
		            if (!$items) {
		                echo '<span class="no-items-mini">No items added</span>';
		            }
		                foreach($items as $item => $values) { 
		                    $_product = $values['data']->post; 
		                    $link = get_permalink($_product);
		                    echo "<span>";
		                        echo $values['quantity']." x <a href='".$link."'>".$_product->post_title;
		                    echo "<a/></span>";
		                    $price = get_post_meta($values['product_id'] , '_price', true);
		                    echo "  Price: ".$currency.$price."<br>";
		                }
		            $total = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );
		            echo "<span class='mini-total'><b>Total: </b>".$currency.$total."</span>";
		        ?>
		    </div>

		    <?php   
		    $mini_cart['.mini-cart-inner'] = ob_get_clean();
		    return $mini_cart;
		}
	}
if ( ! function_exists( 'et_cart_dropdown' ) ) :
	/**
	 *
	 * Get list of product and display it.
	 *
	 */

	function et_cart_dropdown() { ?>
		<?php if ( !is_cart() ) : ?>
		<div class="header-cart">
			<div class="dropdown">
				<h5 class="widget-title">Shopping cart</h5>
				<?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
			</div><!-- End .dropdown -->
		</div>
		<?php endif; 
	}

endif;

	
