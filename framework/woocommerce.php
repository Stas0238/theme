<?php
if ( et_is_woo() ) :
		function et_wc_cart_count($icon = "fa-shopping-cart") {
	 
	        $count = WC()->cart->cart_contents_count; ?>
	        <span class="shop-cart">
	        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
	        <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php esc_html_e( 'View your shopping cart', 'eighttheme' ); ?>"></a>
	       	</span>
	        <?php 
	}
	add_action( 'your_theme_header_top', 'my_wc_cart_count' );
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 ); 
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 11 );

	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_category', 7 ); 
	function woocommerce_template_single_category() {
		$cat = wc_get_product_category_list( get_the_ID () );
		$cat = explode(",", $cat);
		echo '<div class="product-category">' . $cat[0]. '</div>';
	}
	// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	// add_action( 'woocommerce_single_product_summary', 'et_woocommerce_template_single_add_to_cart', 30 );
	// function et_woocommerce_template_single_add_to_cart() { 
	// 	echo '<div class="add-to-cart-and-count"> <form class="cart" method="post" enctype="multipart/form-data">
	// 		<div class="quantity">
	// 			<div class="count-input space-bottom">
 //                    <a class="incr-btn" data-action="decrease" href="#">–</a>
 //                    <input type="text" class="input-text qty text" name="quantity"  step="1" min="1" value="1" size="1" title="Qty" />
 //                    <a class="incr-btn" data-action="increase" href="#">&plus;</a>
 //                </div>
	// 	      </div>
	// 	      <a rel="nofollow" href="'.get_the_permalink().'?add-to-cart='.get_the_ID().'" data-quantity="" data-product_id="'.get_the_ID().'" data-product_sku="" class="single_add_to_cart_button button add_to_cart_button ajax_add_to_cart">Add to cart</a>

	// 		</form></div>';
	// }
	add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
	add_filter( 'woocommerce_product_additional_information_heading', 'remove_product_description_heading' );
	add_filter( 'woocommerce_product_reviews_heading', 'remove_product_description_heading' );
	remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
		function remove_product_description_heading() {
		return '';
		}
	add_filter( 'woocommerce_product_description_tab_title', 'et_product_description_tab_title' );
		function et_product_description_tab_title() {
		    return 'Description Additional';
		}
	add_filter( 'woocommerce_product_additional_information_tab_title', 'et_product_additional_information_tab_title' );
		function et_product_additional_information_tab_title() {
		    return 'Information';
		}
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	add_action( 'wp_ajax_add_foobar', 'prefix_ajax_add_foobar' );
	add_action( 'wp_ajax_nopriv_add_foobar', 'prefix_ajax_add_foobar' );

endif;
 ?>
