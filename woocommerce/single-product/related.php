<?php 
if ( $related_products ) : ?>

	<section class="related products">

		<h2><?php esc_html_e( 'Related products', 'woocommerce' ); ?></h2>
		<?php woocommerce_product_loop_start(); ?>
		<div id="products-carousel" class="owl-carousel owl-theme overflow-navs">
			<?php foreach ( $related_products as $related_product ) : ?>

				<?php
				 	$post_object = get_post( $related_product->get_id() );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					wc_get_template_part( 'content', 'product' ); ?>

			<?php endforeach; ?>
		</div>
		<?php woocommerce_product_loop_end(); ?>

	</section>

<?php endif;
wp_reset_postdata();
 ?>