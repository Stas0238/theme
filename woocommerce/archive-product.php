<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header(); ?>
<div class="content-wrapper">
	    <header class="woocommerce-products-header">

			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

				<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
				<a class="bc-return" href="javascript: history.go(-1)"><i class='fa fa-angle-left' aria-hidden='true'></i>  <?php esc_html_e('Return to Previous Page', 'eighttheme'); ?></a>

				<?php endif; ?>

			<?php
				/**
				 * woocommerce_archive_description hook.
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				do_action( 'woocommerce_archive_description' );
			?>

	    <?php
			/**
			 * woocommerce_before_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 * @hooked WC_Structured_Data::generate_website_data() - 30
			 */
			do_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb' );
		?>
		</header>
		<div class="row">
		<?php get_sidebar( 'shop1' ); ?>
		<?php if (is_active_sidebar('shop-sidebar-left') && is_active_sidebar('shop-sidebar-right')) : ?>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<?php elseif (is_active_sidebar('shop-sidebar-right') || is_active_sidebar('shop-sidebar-left')) :?>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<?php else : ?>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php endif; ?>
			<?php if ( have_posts() ) : ?>

				<?php
					/**
					 * woocommerce_before_shop_loop hook.
					 *
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					?> <div class="products-sorting shop-top-pagination">
					<?php do_action( 'woocommerce_before_shop_loop', et_posts_will_display()); ?>
					<div class="paginating"><?php echo paginate_links(array ('prev_text' => "<i class='fa fa-angle-left' aria-hidden='true'></i>", 'next_text' => "<i class='fa fa-angle-right' aria-hidden='true'></i>")); ?></div>
					</div>

				<div class="row">
				<?php woocommerce_product_loop_start(); ?>

					<?php while ( have_posts() ) : the_post(); ?>
						<?php if (! is_active_sidebar('shop-sidebar-left') && ! is_active_sidebar('shop-sidebar-right')) : ?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<?php elseif(! is_active_sidebar('shop-sidebar-left') || ! is_active_sidebar('shop-sidebar-right')) :?>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-product">
						<?php else :?>
						<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-product">
						<?php endif;

						woocommerce_get_product_thumbnail ();
							/**
							 * woocommerce_shop_loop hook.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							// do_action( 'woocommerce_shop_loop' );
						?>

						<?php wc_get_template_part( 'content', 'product' ); ?>
						</div>

					<?php endwhile; // end of the loop. ?>
				<?php woocommerce_product_loop_end(); ?>
				

				<div class="products-sorting shop-top-pagination">
					<?php do_action( 'woocommerce_before_shop_loop', et_posts_will_display()); ?>
					<div class="paginating"><?php echo paginate_links(array ('prev_text' => "<i class='fa fa-angle-left' aria-hidden='true'></i>", 'next_text' => "<i class='fa fa-angle-right' aria-hidden='true'></i>")); ?></div>
				</div>

			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php
					/**
					 * woocommerce_no_products_found hook.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				?>

			<?php endif; ?>

		<?php
			/**
			 * woocommerce_after_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			 do_action( 'woocommerce_after_main_content' );
		?>
		<?php 
			get_sidebar( 'shop2' ); ?> 
		</div>
</div>
<?php get_footer(); ?>