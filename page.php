<?php 
get_header (); ?>
	<div class="content-wrapper">
<?php if (have_posts()) : ?>
	<div class="row">
		<?php while (have_posts()) : 
			the_post (); ?>
				<div <?php post_class(); ?>>
				<?php the_content (); ?>
				</div>
		<?php endwhile; ?>
	</div> <!-- /row -->
	<?php endif; ?>
	</div>
<?php get_footer ();
?>