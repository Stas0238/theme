<?php 
/* content file */
?>
<article  <?php if (is_single()) { ?> class="post-wrap post-single col-lg-12 col-md-12 col-sm-12" <?php } else { ?> id="post-<?php the_ID (); ?>" <?php post_class (); ?> <?php } ?> >

			<div class="post-thumbnail"> 
				<a href="<?php the_permalink (); ?>"><?php if (is_single()) { the_post_thumbnail('full'); } else { the_post_thumbnail ('large'); } ?> 
				</a>
			</div> 
		<?php if (is_single()) { ?> 
				<h2><?php the_title(); ?></h2>
				<div class="post-data">
					<span class="post-time"><?php the_time ('F n, Y'); echo _e(" at ", "eighttheme"); the_time('H:i'); echo " / ";  ?></span>
					<a class="post-comments" href="#comments_id"><?php echo comments_number (); ?></a>
				</div> 
				<div id="cont" class="post-content"><?php the_content(); ?></div>
				<?php $tags = get_the_tag_list(); if ($tags) : ?>
					<div class="post-tags">
					<?php 	echo $tags; ?>
					</div>
				<?php endif; ?>
				<div class="share-post">
					<div class="share-text"><?php esc_html_e('Share post', 'eighttheme'); ?></div>
						<?php et_share_buttons (); ?>
				</div>
				<div class="post-user">
					<div class="author-avatar">
						<?php et_post_author_avatar_link(); ?>
					</div>
					<div class="author-data">
						<h3 class="author-name"><?php et_post_author_name_link (); ?></h3>
						<div class="author-description"><?php echo get_the_author_meta('description'); ?></div>
					</div>
				</div>
				<?php } else { ?> 
					<p class="post-time"><?php the_time ('F n, Y');?></p>
					<h2><a href="<?php esc_url(the_permalink ()); ?>"><?php the_title (); ?> </a></h2>
					<p class="post-content"><?php echo _e(get_the_excerpt(), 'eighttheme'); ?></p>
				 <?php 
					if ($post->comment_count == 0) { ?>
					<p class="post-comments"><?php echo comments_number (); ?></p>
				<?php } else { ?>
					<p class="post-comments"><a href="<?php esc_url(the_permalink ()); ?>"><?php echo comments_number (); ?></a></p>
				<?php } } ?>
</article> <!-- endarticle -->