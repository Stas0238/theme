<ul class="post-comments">
<?php wp_list_comments('type=comment&callback=et_post_comments'); ?>
</ul>
<div class="add-post share-post">
	<div class="share-text">Add new comments</div>
</div>
<?php /*'author' 	 => ' 
		        		<div class="form-group">
		        			<div class="comment-form-field col-md-4 col-lg-4 col-sm-12 col-xs-12"><input id="author" class="form-controls" type="text" name="name" placeholder="Name (required)" aria-required="true" required="required"></div>
						',   

		    'email'  => '
	                		<div class="comment-form-field col-md-4 col-lg-4 col-sm-12 col-xs-12"><input id="email" type="email" class="form-controls" name="email" placeholder="Email Address (required)" maxlength="100" aria-describedby="email-notes" aria-required="true" required="required"></div>
						' ,

		    'url'    => '
		    			<div class="comment-form-field col-md-4 col-lg-4 col-sm-12 col-xs-12"><input type="text" class="form-controls" name="url" placeholder="Website Address"></div>
						</div>' ) ),

			'comment_field' => '  
		    			<div class="form-group">
							<div class="col-lg-12 comment-area comment-form-field">
 									<textarea id="comment" name="comment" class="form-controls" cols="45" rows="10" aria-required="true" placeholder="Message content (required)" required="required"></textarea>
 								</div>
						</div>', */

		$comments_form = array(
		'class_form'      => 'comment-form row',
	    'id_submit'         => 'comment-submit',
	  	'class_submit'      => 'comment-submit',
	  	'name_submit'       => 'Send message',
	  	'title_reply'       => ( '' ),
	  	'title_reply_to'    => __( 'Leave a Reply to %s' ),
	  	'cancel_reply_link' => __( 'Cancel Reply' ),
	  	'label_submit'      => __( 'Send message' ),
		'comment_notes_before' => '',

		'fields' 	 => apply_filters( 'comment_form_default_fields', array(

		'author' 	 => ' 
		        		<div class="form-group">
		        			<div class="comment-form-field col-md-4 col-lg-4 col-sm-12 col-xs-12"><input id="author" name="author" class="form-controls" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"'  . ' placeholder="Name (required)" required="required" /></div>
						',   

		    'email'  => '
	                		<div class="comment-form-field col-md-4 col-lg-4 col-sm-12 col-xs-12"><input id="email" name="email" class="form-controls" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . ' placeholder="Email Address (required)" required="required" /></div>
						' ,

		    'url'    => '
		    			<div class="comment-form-field col-md-4 col-lg-4 col-sm-12 col-xs-12"><input id="url" name="url" class="form-controls" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . ' placeholder="Website Address "  /></div>
						</div>' ) ),

			'comment_field' => '  
		    			<div class="form-group">
							<div class="col-lg-12 comment-area comment-form-field">
 									<textarea id="comment" name="comment" class="form-controls" cols="45" rows="10" aria-required="true" placeholder="Message content (required)" required="required"></textarea>
 								</div>
						</div>',

		);
		comment_form($comments_form);
?>