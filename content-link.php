<?php 
/* content file */
?>
<article  <?php if (is_single()) { ?> class="post-wrap post-single col-lg-12 col-md-12 col-sm-12" <?php } else { ?> id="post-<?php the_ID (); ?>" <?php post_class (); ?> <?php } ?> >
			<h2><a href="<?php esc_url(the_permalink ()); ?>"><?php the_title (); ?> </a></h2>
</article> <!-- endarticle -->