<?php
wp_footer ();
?>
</div> <!-- /container -->
</div>
<div id="prefooter">
	<?php dynamic_sidebar('prefooter');?> 
</div>
<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<p class="logo"><?php et_get_logo(); ?></p>
				<?php dynamic_sidebar('footer-area-1'); ?>
				<div class="social-widgets">
					<a href="#"></a>
					<a href="#"></a>
					<a href="#"></a>
					<a href="#"></a>
					<a href="#"></a>
				</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<?php dynamic_sidebar('footer-area-2'); ?>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<?php dynamic_sidebar('footer-area-3'); ?>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<?php dynamic_sidebar('footer-area-4'); ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
		</div>
	</div>
</div>
<footer id="footer-bottom">
	<?php dynamic_sidebar('footer-copyrights'); ?>
</footer>
</body>
</html>