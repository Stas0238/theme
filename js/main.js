jQuery(document).ready(function($) {
	"use strict";
		if ( $( ".row" ).hasClass( 'stretch-row' ) ) {
			var bodyWidth = document.body.offsetWidth;
			var containerMargin = (bodyWidth - document.getElementsByClassName( "container" )[0].offsetWidth) / 2 - 15;
			// console.log(containerMargin);
			// console.log(bodyWidth);
			var elWidth = document.getElementsByClassName( "stretch-row" )[0].offsetWidth;
			var diff = (bodyWidth - elWidth) /-2;
			// console.log(diff);
			$( ".stretch-row" ).css({
				width: bodyWidth,
				position: 'relative',
				boxSizing: 'border-box',
				left: diff,
				paddingLeft: containerMargin,
				paddingRight: containerMargin
			});
			jQuery( ".testimonial-content h4" ).remove();
			jQuery.each( jQuery( '.quote' ), function() {
			  console.log('test');
			  var text = jQuery(this).find( '.testimonial-content' );
			  //if (  $(this).find('.avtar-image') ) {
			   jQuery(this).find( '.testimonial-content' ).remove();
			   jQuery(this).find( '.testimonial-left' ).prepend(text);
			  })
  			}
  			jQuery(".single-product .summary .cart").wrap("<div class='add-to-cart-and-count'></div>");
  			jQuery(".single-product .summary input[type=number].qty").wrap("<div class='count-input space-bottom'></div>");
  			jQuery('.single-product .summary input[type=number].qty').attr("size" , "1");
  			jQuery(".single-product .summary .count-input").prepend(" <a class='incr-btn' data-action='decrease' href='#'>–</a>");
  			jQuery(".single-product .summary .count-input").append(" <a class='incr-btn' data-action='increase' href='#'>&plus;</a>");
  			 $(".incr-btn").on("click", function (e) {
	        var $button = $(this);
	        var oldValue = $button.parent().find('.qty').val();
	        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
	        if ($button.data('action') == "increase") {
	            var newVal = parseFloat(oldValue) + 1;
	        } else {
	            // Don't allow decrementing below 1
	            if (oldValue > 1) {
	                var newVal = parseFloat(oldValue) - 1;
	            } else {
	                newVal = 1;
	                $button.addClass('inactive');
	            }
	        }
	        $button.parent().find('.qty').val(newVal);
	        e.preventDefault();
	    });
  			 jQuery( ".flex-control-nav.flex-control-thumbs" ).addClass("owl-carousel owl-theme overflow-navs");
  			 jQuery('.flex-control-nav').wrap('<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1"></div>');
  			 jQuery(".flex-control-thumbs").owlCarousel({
		 
		      autoPlay: 3000, //Set AutoPlay to 3 seconds
		 	  margin: 15,
		      items : 3,
		      itemsDesktop : [1199,3],
		      itemsDesktopSmall : [979,3],
		      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		      nav: true,
		      dots : false,
		 
		  });
  			 jQuery(".woocommerce-product-gallery__trigger img").remove();
  			 jQuery(".woocommerce-product-gallery__trigger").html('<i class="fa fa-arrows-alt" aria-hidden="true"></i>');
  			 var commentRespond = jQuery("#respond .comment-form-comment").remove();
  			 jQuery("#respond .form-submit").prepend(commentRespond);

  			 var woo_message = jQuery(".woocommerce-page .products-sorting .woocommerce-message").remove();
  			 jQuery(".woocommerce-page .products-sorting").before(woo_message);
  			 jQuery(".single-product button.single_add_to_cart_button.button");
  			 jQuery(".single-product .single_add_to_cart_button").attr("data-quantity", "1");
  			 var value = jQuery(".single-product .single_add_to_cart_button").attr("value");
  			 jQuery(".single-product .single_add_to_cart_button").removeAttr('value');
  			  jQuery(".single-product .single_add_to_cart_button").attr("data-product_id", value);
  			 jQuery(".single-product .single_add_to_cart_button").removeAttr('name');
  			 var ajs = document.getElementsByClassName("single_add_to_cart_button")[0];
  			 jQuery(ajs).addClass("add_to_cart_button ajax_add_to_cart");
  			 jQuery('.single_add_to_cart_button').click(function () {
		  			var button = document.getElementsByClassName('qty')[0];
					console.log(button);
					var attr = jQuery(button).attr("value");
					console.log(attr);
					jQuery('.single_add_to_cart_button').attr("data-quantity" , attr);
			 });

 });