<?php 
get_header (); ?>
<div class="content-wrapper">
	<div class="row">
<?php if (have_posts()) : ?>
		<?php while (have_posts()) : 
			the_post (); ?>
				<?php the_content (); ?>
		<?php endwhile; ?>
	</div> <!-- /row -->
	<?php endif; ?>
</div>
<?php get_footer ();
?>