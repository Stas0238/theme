<button class="search-form" id="searchsubmit" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search" aria-hidden="true"></i></button>
<div id="searchModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
	        <h4 class="modal-title"><?php _e("What are you looking for ?", "eightheme"); ?></h4>
	      </div>
	      <div class="modal-body">
	        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url(); ?>">
		        <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="search">
		        <input type="hidden" name="<?php if (class_exists('WooCommerce')) echo 'products'; else { echo 'posts'; } ?>"> 
		        <input type="submit" id="searchsubmit" value="Search">
	        </form>
	      </div>
	    </div>

	  </div>
	</div>